package com.example.medicapp.database

import androidx.room.*
import com.example.medicapp.models.User

@Dao
interface UserDao{

    //Aqui se persiste un usuario
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(user:User)

    //traer los usuarios que se desean
    @Query("SELECT * FROM user")
    fun getAllUsers(): Array<User>

    //seleccionar un unico usuario que se quiere mostrar
    @Query("SELECT * FROM user LIMIT 1")
    fun getUser(): User

    //actualizar en la persistencia la info de un usuario
    @Update
    fun updateUser(user: User)

    //GET del usuario qeu tenga el nombre pasado por parametro
    @Query("SELECT * FROM user WHERE firstName = :name")
    fun getUserByName(name: String): User

    //GET del usuario con el telefono especificado
    @Query("SELECT * FROM user WHERE phoneNumber = :phone")
    fun getUserByPhone(phone: String): User

    //eliminar un usuario
    @Delete
    fun deleteUser(user: User)


    @Query("UPDATE user SET heartRate = :pulse WHERE phoneNumber = :phoneNum")
    fun updateUserCPById(phoneNum: String, pulse: Double)


}
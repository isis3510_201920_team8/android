package com.example.medicapp.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "medicine_table")
data class Medicine(
    @PrimaryKey(autoGenerate = true)
    var medicineId: Long = 0L,

    @ColumnInfo(name = "hour_medicine")
    var hour: Int = 0,

    @ColumnInfo(name = "minute_medicine")
    var minute: Int = 0,

    @ColumnInfo(name = "name_medicine")
    var name: String = "",

    @ColumnInfo(name = "monday_medicine")
    var monday: Boolean = false,

    @ColumnInfo(name = "tuesday_medicine")
    var tuesday: Boolean = false,

    @ColumnInfo(name = "wednesday_medicine")
    var wednesday: Boolean = false,

    @ColumnInfo(name = "thursday_medicine")
    var thursday: Boolean = false,

    @ColumnInfo(name = "friday_medicine")
    var friday: Boolean = false,

    @ColumnInfo(name = "saturday_medicine")
    var saturday: Boolean = false,

    @ColumnInfo(name = "sunday_medicine")
    var sunday: Boolean = false
)
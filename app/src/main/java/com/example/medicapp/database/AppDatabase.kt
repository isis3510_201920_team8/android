package com.example.medicapp.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.medicapp.models.User
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.google.firebase.firestore.FirebaseFirestore
import android.icu.lang.UCharacter.GraphemeClusterBreak.V
import android.util.Log


@Database(
    entities = [(User::class)],
    version = 2,
    exportSchema = false
)

//logica para la persistencia
abstract class AppDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao

    companion object {
        var INSTANCE: AppDatabase? = null

        //conexion a la base de datos
        fun getAppDataBase(context: Context): AppDatabase? {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext, AppDatabase::class.java, "medicapp")
                        .allowMainThreadQueries().fallbackToDestructiveMigration().build()
                    val firestore = FirebaseFirestore.getInstance()
                    val settings = FirebaseFirestoreSettings.Builder()
                        .setTimestampsInSnapshotsEnabled(true)
                        .build()
                    firestore.firestoreSettings = settings
                }
            }
            return INSTANCE
        }

        fun destroyDataBase() {
            INSTANCE = null
        }
    }



}



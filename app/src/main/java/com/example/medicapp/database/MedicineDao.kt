package com.example.medicapp.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update


@Dao
interface MedicineDao{

    @Insert
    fun insert(medicine: Medicine)

    @Update
    fun update(medicine: Medicine)

    @Query("SELECT * from medicine_table WHERE medicineId = :key")
    fun get(key: Long): Medicine?

    @Query("DELETE FROM medicine_table")
    fun clear()

    @Query("DELETE FROM medicine_table WHERE medicineId = :key")
    fun delete(key: Long)

    @Query("SELECT * FROM medicine_table ORDER BY medicineId DESC")
    fun getAllMedicaments(): LiveData<List<Medicine>>
}

package com.example.medicapp.ui.addMedicine

import android.app.Application
import android.app.TimePickerDialog
import android.view.View
import android.widget.EditText
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.medicapp.database.Medicine
import com.example.medicapp.database.MedicineDao
import kotlinx.coroutines.*
import java.text.SimpleDateFormat
import java.util.*

class AddMedicineViewModel(val database: MedicineDao,
                         application: Application
) : AndroidViewModel(application)  {
    // TODO: Implement the

    var timeString =MutableLiveData<String>()
    var medicineNameString =MutableLiveData<String>()
    var hour =MutableLiveData<Int>()
    var minute =MutableLiveData<Int>()
    var medNamee :String=""

    var mondayCheck =MutableLiveData<Boolean>()
    var tuesdayCheck =MutableLiveData<Boolean>()
    var wednesdayCheck =MutableLiveData<Boolean>()
    var thursdayCheck =MutableLiveData<Boolean>()
    var fridayCheck =MutableLiveData<Boolean>()
    var saturdayCheck =MutableLiveData<Boolean>()
    var sundayCheck =MutableLiveData<Boolean>()

    fun onMondayCheck() {
        if(mondayCheck.value==true){
            mondayCheck.value=false
        }
        else{
            mondayCheck.value=true
        }
    }

    fun onTuesdayCheck() {
        if(tuesdayCheck.value==true){
            tuesdayCheck.value=false
        }
        else{
            tuesdayCheck.value=true
        }
    }

    fun onWednesdayCheck() {
        if(wednesdayCheck.value==true){
            wednesdayCheck.value=false
        }
        else{
            wednesdayCheck.value=true
        }
    }

    fun onThursdayCheck() {
        if(thursdayCheck.value==true){
            thursdayCheck.value=false
        }
        else{
            thursdayCheck.value=true
        }
    }

    fun onFridayCheck() {
        if(fridayCheck.value==true){
            fridayCheck.value=false
        }
        else{
            fridayCheck.value=true
        }
    }

    fun onSaturdayCheck() {
        if(saturdayCheck.value==true){
            saturdayCheck.value=false
        }
        else{
            saturdayCheck.value=true
        }
    }

    fun onSundayCheck() {
        if(sundayCheck.value==true){
            sundayCheck.value=false
        }
        else{
            sundayCheck.value=true
        }
    }

    fun changeTimeString(time:String, hour:Int,minute:Int) {
        timeString.value=time
        this.hour.value=hour
        this.minute.value=minute
    }

    fun changeMedicineNameString(name:String) {
        medicineNameString.value=name
    }

    private val _navigateToDashboard =
        MutableLiveData<Medicine>()
    val navigateToDashboard: LiveData<Medicine>
        get() = _navigateToDashboard

    fun doneNavigating() {
        _navigateToDashboard.value = null
    }

    private var _showSnackbarEvent = MutableLiveData<Boolean>()

    val showSnackBarEvent: LiveData<Boolean>
        get() = _showSnackbarEvent

    fun doneShowingSnackbar() {
        _showSnackbarEvent.value = false
    }

    private var viewModelJob = Job()
    private val uiScope =
        CoroutineScope(Dispatchers.Main + viewModelJob)

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    fun onStartTracking() {
        uiScope.launch {
            var newMed = Medicine()
            newMed.name=medNamee
            newMed.hour= hour.value!!
            newMed.minute= minute.value!!
            if(mondayCheck.value!=null){
                newMed.monday=mondayCheck.value!!
            }
            if(tuesdayCheck.value!=null){
                newMed.tuesday=tuesdayCheck.value!!
            }
            if(wednesdayCheck.value!=null){
                newMed.wednesday=wednesdayCheck.value!!
            }
            if(thursdayCheck.value!=null){
                newMed.thursday=thursdayCheck.value!!
            }
            if(fridayCheck.value!=null){
                newMed.friday=fridayCheck.value!!
            }
            if(saturdayCheck.value!=null){
                newMed.saturday=saturdayCheck.value!!
            }
            if(sundayCheck.value!=null){
                newMed.sunday=sundayCheck.value!!
            }
            insert(newMed)
            _navigateToDashboard.value = newMed
        }
    }

    fun onAddMedicine() {
        uiScope.launch {
            _showSnackbarEvent.value=true
        }
    }

    private suspend fun insert(med: Medicine) {
        withContext(Dispatchers.IO) {
            database.insert(med)
        }
    }

}
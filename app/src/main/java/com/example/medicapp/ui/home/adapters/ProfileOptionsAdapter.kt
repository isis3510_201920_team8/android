package com.example.medicapp.ui.home.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.navigation.Navigation.findNavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.medicapp.R
import com.example.medicapp.entities.ProfileOption
import com.example.medicapp.ui.home.constants.ProfileConstants
import com.google.android.material.button.MaterialButton

class ProfileOptionsAdapter(
    private val elements: List<ProfileOption>?,
    private val callback: ProfileOptionsInteraction
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            ProfileConstants.IMAGE_PROFILE_TYPE -> ImageOptionViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.profile_option_image_item, parent, false)
            )
            ProfileConstants.TEXT_BUTTON_PROFILE_TYPE -> TextButtonOptionViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.profile_option_button_item, parent, false)
            )
            else -> TextOptionViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.profile_option_item, parent, false)
            )
        }
    }

    override fun getItemCount() = elements?.size ?: 0

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder.itemViewType) {
            ProfileConstants.TEXT_PROFILE_TYPE -> {
                (holder as TextOptionViewHolder).bind(elements?.get(position))
            }
            ProfileConstants.TEXT_BUTTON_PROFILE_TYPE -> {
                (holder as TextButtonOptionViewHolder).bind(elements?.get(position))
            }
            ProfileConstants.IMAGE_PROFILE_TYPE -> {
                (holder as ImageOptionViewHolder).bind(elements?.get(position), callback)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return elements?.get(position)?.profileOptionType ?: ProfileConstants.IMAGE_PROFILE_TYPE
    }

    /**
     * View Holder class that represents the profile options cells that only have information on the left side.
     */
    class TextOptionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var profileOptionCategory: TextView? = null
        private var profileOptionValue: TextView? = null

        init {
            profileOptionCategory = itemView.findViewById(R.id.tv_profile_option_category_profile)
            profileOptionValue = itemView.findViewById(R.id.tv_profile_option_value_profile)
        }

        fun bind(profileOption: ProfileOption?) {
            profileOptionCategory?.text = profileOption?.category
            profileOptionValue?.text = profileOption?.value
        }
    }

    class TextButtonOptionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var profileOptionCategory: TextView? = null
        private var profileOptionValue: TextView? = null
        private var profileOptionButton: MaterialButton? = null

        init {
            profileOptionCategory = itemView.findViewById(R.id.tv_profile_option_category_profile)
            profileOptionValue = itemView.findViewById(R.id.tv_profile_option_value_profile)
            profileOptionButton = itemView.findViewById(R.id.btn_profile_option_action)
        }

        fun bind(profileOption: ProfileOption?) {
            profileOptionCategory?.text = profileOption?.category
            profileOptionValue?.text = profileOption?.value
            profileOptionButton?.setOnClickListener {
                Log.d("CLICK", "CLICK ME")
                findNavController(itemView).navigate(R.id.action_profileFragment_to_cartoidPulseFragment)
            }
        }
    }

    class ImageOptionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var profileOptionImage: ImageView? = null

        init {
            profileOptionImage = itemView.findViewById(R.id.iv_profile_option_image)
        }

        fun bind(profileOption: ProfileOption?, callback: ProfileOptionsInteraction) {
            profileOptionImage?.background = callback.getApplicationContext()?.let {
                profileOption?.image?.let { it1 ->
                    ContextCompat.getDrawable(
                        it,
                        it1
                    )
                }
            }
        }
    }

    interface ProfileOptionsInteraction {
        /**
         * Gets context of the activity or fragment that called this adapter.
         * @return the context of the current screen.
         */
        fun getApplicationContext(): Context?
    }
}
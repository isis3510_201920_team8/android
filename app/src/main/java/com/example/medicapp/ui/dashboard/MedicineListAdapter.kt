package com.example.medicapp.ui.dashboard

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.medicapp.R
import com.example.medicapp.database.Medicine

class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
    val medicineNeme: TextView = itemView.findViewById(R.id.sleep_length)
    val hourLabel: TextView = itemView.findViewById(R.id.quality_string)
    val pillImage: ImageView = itemView.findViewById(R.id.quality_image)
    val mondayLabel: TextView = itemView.findViewById(R.id.textViewL)
    val tuesdayLabel: TextView = itemView.findViewById(R.id.textViewT)
    val wednesdayLabel: TextView = itemView.findViewById(R.id.textViewW)
    val thursdayLabel: TextView = itemView.findViewById(R.id.textViewTh)
    val fridayLabel: TextView = itemView.findViewById(R.id.textViewF)
    val saturdayLabel: TextView = itemView.findViewById(R.id.textViewS)
    val sundayLabel: TextView = itemView.findViewById(R.id.textViewSu)

}

class MedicineListAdapter: RecyclerView.Adapter<ViewHolder>() {
    var data =  listOf<Medicine>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }
    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = data[position]
        holder.medicineNeme.text = item.name
        holder.hourLabel.text= item.hour.toString() +":"+ item.minute.toString()
        holder.pillImage.setImageResource(R.drawable.drugs)
        if(item.monday){
            holder.mondayLabel.setBackgroundColor(Color.GREEN)
        }
        if(item.tuesday){
            holder.tuesdayLabel.setBackgroundColor(Color.GREEN)
        }
        if(item.wednesday){
            holder.wednesdayLabel.setBackgroundColor(Color.GREEN)
        }
        if(item.thursday){
            holder.thursdayLabel.setBackgroundColor(Color.GREEN)
        }
        if(item.friday){
            holder.fridayLabel.setBackgroundColor(Color.GREEN)
        }
        if(item.saturday){
            holder.saturdayLabel.setBackgroundColor(Color.GREEN)
        }
        if(item.sunday){
            holder.sundayLabel.setBackgroundColor(Color.GREEN)
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater =
            LayoutInflater.from(parent.context)
        val view = layoutInflater
            .inflate(R.layout.list_item_medicine,
                parent, false)
        return ViewHolder(view)
    }
}

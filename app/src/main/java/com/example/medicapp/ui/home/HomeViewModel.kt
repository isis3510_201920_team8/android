package com.example.medicapp.ui.home

import android.util.Log
import androidx.constraintlayout.solver.widgets.Snapshot
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.medicapp.R
import com.example.medicapp.entities.ProfileOption
import com.example.medicapp.models.User
import com.example.medicapp.repository.user.UserDataSource
import com.example.medicapp.ui.home.constants.ProfileConstants
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import kotlin.math.log

class HomeViewModel : ViewModel() {



    /**
     * A fields that connects to the data source where all data is retrieved or updated.
     */
    private val userDataSource = UserDataSource()

    /**
     * The user response live data that activates whenever a change has been made.
     */
    val userResponse = userDataSource.response


    /**
     * Builds a dummy disease list to fill the drop down.
     * @return a list with the diseases to be displayed.
     */
    fun getDiseasesList(): List<String> {
        return listOf("Migraña", "Hipertensión", "Diabetes", "Alzheimer", "Otra")
    }

    /**
     *
     */
    var phone: String = ""
        //set(value){field = value}

    /*
     *
     */
    fun setPhoneTest(p: String){

        phone = p
    }


    override fun onCleared() {
        super.onCleared()
    }



    /**
     * Tells the data source to register the user with all of the arguments.
     * @param firstName the first name of the user.
     * @param lastName the last name of the user.
     * @param phoneNumber the phone number of the user.
     * @param eps the eps of the user.
     * @param disease the disease that suffers the user.
     */
    fun registerUserData(
        firstName: String,
        lastName: String,
        phoneNumber: String,
        eps: String,
        disease: String
    ) {
        userDataSource.registerUserData(firstName, lastName, phoneNumber, eps, disease)
    }


    /**
     *
     */
    fun getUser(): User? {
        return userDataSource.getUser()
    }

    /**
     *
     */
    fun isUserLogged(): Boolean {
        return userDataSource.isUserLogged()
    }

    /**
     *
     */
    fun logoutUser() {
        userDataSource.logoutUser()
    }

    /**
     *
     */
    fun updateUserCPById(pulse: Double) {
        userDataSource.updateUserCPById(getUser()?.phoneNumber ?: "", pulse)
    }

    /**
     *
     */
    private fun getUserByPhone(): User?{
        return userDataSource.getUser(phone)
    }

    /**
     *
     */
    fun getProfileOptionList(): List<ProfileOption> {


        var user: User?
        if (phone == ""){
            user = getUser()

        }else{
            user= getUserByPhone()
            Log.d("aaaaaa", "user ${user.toString()}")
        }
        val profileList = mutableListOf<ProfileOption>()
        val image = ProfileOption("", "", R.drawable.user_pic, ProfileConstants.IMAGE_PROFILE_TYPE)
        val fullName = ProfileOption(
            "Nombre completo",
            user?.firstName + " " + user?.lastName,
            -1,
            ProfileConstants.TEXT_PROFILE_TYPE
        )
        val phoneNumber = ProfileOption(
            "Número de teléfono",
            user?.phoneNumber,
            -1,
            ProfileConstants.TEXT_PROFILE_TYPE
        )
        val eps = ProfileOption("EPS", user?.eps, -1, ProfileConstants.TEXT_PROFILE_TYPE)
        val disease =
            ProfileOption("Síntomas", user?.disease, -1, ProfileConstants.TEXT_PROFILE_TYPE)
        val heartRate = ProfileOption(
            "Ritmo cardíaco",
            user?.heartRate.toString(),
            -1,
            ProfileConstants.TEXT_BUTTON_PROFILE_TYPE
        )
        val bloodPressure = ProfileOption(
            "Presión arterial",
            user?.bloodPressure.toString(),
            -1,
            ProfileConstants.TEXT_BUTTON_PROFILE_TYPE
        )
        val saturation = ProfileOption(
            "Saturación",
            user?.saturation.toString(),
            -1,
            ProfileConstants.TEXT_BUTTON_PROFILE_TYPE
        )
        profileList.add(image)
        profileList.add(fullName)
        profileList.add(phoneNumber)
        profileList.add(eps)
        profileList.add(disease)
        profileList.add(heartRate)
        profileList.add(bloodPressure)
        profileList.add(saturation)
        return profileList


    }

   /* private fun getOG( phoneNumber: String): MutableList<ProfileOption> {


        val db = FirebaseFirestore.getInstance()
        val profileList = mutableListOf<User>()
        val profileList1 = mutableListOf<ProfileOption>()

        db.collection("patients").get()
            .addOnSuccessListener { queryDocumentSnapshots ->

                val list = queryDocumentSnapshots.documents
                println(list.size)
                for (d in list) {

                    val p = d.toObject(User::class.java!!)

                    if (p?.phoneNumber == phoneNumber) {
                        val profileList1 = mutableListOf<ProfileOption>()

                        val img = ProfileOption("", "", R.drawable.user_pic, ProfileConstants.IMAGE_PROFILE_TYPE)
                        val fullName = ProfileOption(
                            "Nombre completo",
                            p?.firstName + " " + p?.lastName,
                            -1,
                            ProfileConstants.TEXT_PROFILE_TYPE
                        )
                        val phoneNumber = ProfileOption(
                            "Número de teléfono",
                            p.phoneNumber,
                            -1,
                            ProfileConstants.TEXT_PROFILE_TYPE
                        )

                        val eps = ProfileOption("EPS", p.eps, -1, ProfileConstants.TEXT_PROFILE_TYPE)
                        val disease =
                            ProfileOption("Síntomas", p.disease, -1, ProfileConstants.TEXT_PROFILE_TYPE)
                        val heartRate = ProfileOption(
                            "Ritmo cardíaco",
                            p.heartRate.toString(),
                            -1,
                            ProfileConstants.TEXT_BUTTON_PROFILE_TYPE
                        )
                        val bloodPressure = ProfileOption(
                            "Presión arterial",
                            p.bloodPressure.toString(),
                            -1,
                            ProfileConstants.TEXT_BUTTON_PROFILE_TYPE
                        )
                        val saturation = ProfileOption(
                            "Saturación",
                            p.saturation.toString(),
                            -1,
                            ProfileConstants.TEXT_BUTTON_PROFILE_TYPE
                        )
                        profileList1.add(img)
                        profileList1.add(fullName)
                        profileList1.add(phoneNumber)
                        profileList1.add(eps)
                        profileList1.add(disease)
                        profileList1.add(heartRate)
                        profileList1.add(bloodPressure)
                        profileList1.add(saturation)

                        break
                    }


                }




            }
        return profileList1

    }*/
}
package com.example.medicapp.ui.home

import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.example.medicapp.MainActivity
import com.example.medicapp.R
import com.example.medicapp.repository.user.UserConstants
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputEditText


class HomeFragment : Fragment() {

    private val etFirstName by lazy<TextInputEditText?> {
        view?.findViewById(R.id.tie_first_name_home)
    }

    private val etLastName by lazy<TextInputEditText?> {
        view?.findViewById(R.id.tie_last_name_home)
    }

    private val etPhone by lazy<TextInputEditText?> {
        view?.findViewById(R.id.tie_phone_home)
    }

    private val etEps by lazy<TextInputEditText?> {
        view?.findViewById(R.id.tie_eps_home)
    }

    private val ddDiseases by lazy<AutoCompleteTextView?> {
        view?.findViewById(R.id.ac_diseases_home)
    }

    private val btnEnterData by lazy<MaterialButton?> {
        view?.findViewById(R.id.btn_enter_data_home)
    }

    private val btnLogIn by lazy<MaterialButton?> {
        view?.findViewById(R.id.btn_login)
    }

    private var homeViewModel: HomeViewModel? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProviders.of(this).get(HomeViewModel::class.java)
        if(homeViewModel?.isUserLogged() == true){
            findNavController().navigate(R.id.action_navigation_home_to_profileFragment)
        }


        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    /**
     *
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (homeViewModel?.isUserLogged() == true) {
            handleUIIfUserIsLoggedIn()
        } else {
            btnLogIn?.visibility = View.VISIBLE
            setUpDiseasesDropdown()
            setObservers()
        }
        setOnClickListeners()
    }

    /**
     * Fills the dropdown list with some dummy diseases.
     */
    private fun setUpDiseasesDropdown() {
        context?.let {
            val adapter = ArrayAdapter(
                it,
                R.layout.dropdown_menu_item,
                homeViewModel?.getDiseasesList() ?: emptyList()
            )
            ddDiseases?.setAdapter(adapter)
        }
    }

    /**
     * Sets the observers that handles all the responses from the view model and repository.
     */
    private fun setObservers() {
        homeViewModel?.userResponse?.observe(this, Observer {
            when(it) {
                UserConstants.USER_CREATED ->{
                    findNavController().navigate(R.id.action_navigation_home_to_profileFragment)
                    if (activity is MainActivity) (activity as MainActivity).showSnackBar(getString(
                        R.string.user_was_already_created))
                }
                UserConstants.USER_ALREADY_EXISTS ->
                    if (activity is MainActivity) (activity as MainActivity).showSnackBar(getString(
                        R.string.user_already_exists))
                UserConstants.USER_NOT_CREATED_FAILURE ->
                    if (activity is MainActivity) (activity as MainActivity).showSnackBar(getString(
                        R.string.there_was_a_server_error_when_creating_the_user))

            }
        })
    }

    /**
     * Creates the click listeners for the elements in the UI.
     */
    private fun setOnClickListeners() {
        btnEnterData?.setOnClickListener { validateData() }

        btnLogIn?.setOnClickListener {
            findNavController().navigate(R.id.action_navigation_home_to_loginFragment)
        }
    }

    /**
     *
     */
    private fun handleUIIfUserIsLoggedIn() {
        etFirstName?.isEnabled = false
        etLastName?.isEnabled = false
        etPhone?.isEnabled = false
        etEps?.isEnabled = false
        ddDiseases?.isEnabled = false
        btnEnterData?.isClickable = false
        btnEnterData?.isFocusable = false
        btnLogIn?.visibility = View.VISIBLE
    }

    /**
     * Checks if fields are empty and shows a Toast, if they are not empty, we should try to register the user.
     */
    private fun validateData() {
        if (!TextUtils.isEmpty(etFirstName?.text) && !TextUtils.isEmpty(etLastName?.text) &&
            !TextUtils.isEmpty(etPhone?.text) && !TextUtils.isEmpty(etEps?.text) &&
            !TextUtils.isEmpty(ddDiseases?.text)) {
            homeViewModel?.registerUserData(
                etFirstName?.text.toString(),
                etLastName?.text.toString(),
                etPhone?.text.toString(),
                etEps?.text.toString(),
                ddDiseases?.text.toString()
            )
        } else {
            if (activity is MainActivity) (activity as MainActivity).showSnackBar(getString(R.string.check_fields_are_not_empty))
        }
    }
}
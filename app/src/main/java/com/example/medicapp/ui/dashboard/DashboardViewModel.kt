package com.example.medicapp.ui.dashboard

import android.app.Application
import android.content.res.Resources
import android.os.Build
import android.text.Html
import android.text.Spanned
import android.util.Log
import androidx.core.text.HtmlCompat
import androidx.lifecycle.*
import com.example.medicapp.database.Medicine
import com.example.medicapp.database.MedicineDao
import com.example.medicapp.formatMeds
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch


class DashboardViewModel(val database: MedicineDao,
                         application: Application
) : AndroidViewModel(application)  {
    // TODO: Implement the

    private val _text = MutableLiveData<String>().apply {
        value = "Pastillero "
    }
    val text: LiveData<String> = _text

    private val _navigateToAddMedicine =
        MutableLiveData<Medicine>()
    val navigateToAddMedicine: LiveData<Medicine>
        get() = _navigateToAddMedicine

    fun doneNavigating() {
        _navigateToAddMedicine.value = null
    }

    private var viewModelJob = Job()
    private val uiScope =
        CoroutineScope(Dispatchers.Main + viewModelJob)

    val medicines = database.getAllMedicaments()

    var medsString = Transformations.map(medicines) { meds ->
        formatMeds(meds, application.resources)
        Log.d("hola",medicines.toString())
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    fun onStartTracking() {
        uiScope.launch {
            _navigateToAddMedicine.value = Medicine()
        }
    }

}
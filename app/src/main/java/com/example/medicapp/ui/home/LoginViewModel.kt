package com.example.medicapp.ui.home

import androidx.lifecycle.ViewModel
import com.example.medicapp.R
import com.example.medicapp.entities.ProfileOption
import com.example.medicapp.models.User
import com.example.medicapp.repository.user.UserDataSource
import com.example.medicapp.ui.home.constants.ProfileConstants

class LoginViewModel : ViewModel() {
    // TODO: Implement the ViewModel

    /**
     *
     */
    private val userDataSource = UserDataSource()

    /**
    * The user response live data that activates whenever a change has been made.
    */
    val userResponse = userDataSource.response

    var phoneNum:String = ""

    /**
     *
     */
    fun getUser(): User? {
        return userDataSource.getUser(phoneNum)
    }


    /**
     *
     */
    fun getProfileOptionList(): List<ProfileOption> {

        val profileList = mutableListOf<ProfileOption>()
        val user= getUser()
        val image = ProfileOption("", "", R.drawable.user_pic, ProfileConstants.IMAGE_PROFILE_TYPE)
        val fullName = ProfileOption(
            "Nombre completo",
            user?.firstName + " " + user?.lastName,
            -1,
            ProfileConstants.TEXT_PROFILE_TYPE
        )
        val phoneNumber = ProfileOption(
            "Número de teléfono",
            user?.phoneNumber,
            -1,
            ProfileConstants.TEXT_PROFILE_TYPE
        )
        val eps = ProfileOption("EPS", user?.eps, -1, ProfileConstants.TEXT_PROFILE_TYPE)
        val disease =
            ProfileOption("Síntomas", user?.disease, -1, ProfileConstants.TEXT_PROFILE_TYPE)
        val heartRate = ProfileOption(
            "Ritmo cardíaco",
            user?.heartRate.toString(),
            -1,
            ProfileConstants.TEXT_BUTTON_PROFILE_TYPE
        )
        val bloodPressure = ProfileOption(
            "Presión arterial",
            user?.bloodPressure.toString(),
            -1,
            ProfileConstants.TEXT_BUTTON_PROFILE_TYPE
        )
        val saturation = ProfileOption(
            "Saturación",
            user?.saturation.toString(),
            -1,
            ProfileConstants.TEXT_BUTTON_PROFILE_TYPE
        )
        profileList.add(image)
        profileList.add(fullName)
        profileList.add(phoneNumber)
        profileList.add(eps)
        profileList.add(disease)
        profileList.add(heartRate)
        profileList.add(bloodPressure)
        profileList.add(saturation)
        return profileList

        //return lista
    }
}

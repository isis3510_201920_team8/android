package com.example.medicapp.ui.home.constants

object ProfileConstants {
    const val IMAGE_PROFILE_TYPE = 0
    const val TEXT_PROFILE_TYPE = 1
    const val TEXT_BUTTON_PROFILE_TYPE = 2
}
package com.example.medicapp.ui.home

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.medicapp.R
import com.example.medicapp.ui.home.adapters.ProfileOptionsAdapter
import com.google.android.material.button.MaterialButton

class ProfileFragment() : Fragment(), ProfileOptionsAdapter.ProfileOptionsInteraction {



    private val btnLogout by lazy<ImageView?>{
        view?.findViewById(R.id.iv_log_out_profile)
    }

    private val rvOptions by lazy<RecyclerView?> {
        view?.findViewById(R.id.rv_profile_option_list)
    }

    private val btnMeasureCp by lazy<MaterialButton?>{
        view?.findViewById(R.id.btn_profile_option_action)
    }


    private var viewModel: HomeViewModel? = null
    //private var loginViewModel: LoginViewModel? = null

    override fun onCreateView(
        inflater: LayoutInflater, @Nullable container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        var phone= arguments?.getString("phone")
       viewModel =
            ViewModelProviders.of(this).get(HomeViewModel::class.java)

        if(phone != null)
            viewModel?.phone= phone

        //loginViewModel= ViewModelProviders.of(this).get(LoginViewModel::class.java)

        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setTextsWithUserInformation()
        setOnClickListeners()
        //setTextsWithUserInformation2()
    }

    private fun setTextsWithUserInformation() {
        rvOptions?.apply {

            adapter = ProfileOptionsAdapter(viewModel?.getProfileOptionList(), this@ProfileFragment)
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        }
    }

    /*private fun setTextsWithUserInformation2(){
        rvOptions?.apply {
            adapter = ProfileOptionsAdapter(loginViewModel?.getProfileOptionList(), this@ProfileFragment)
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        }
    }*/


    private fun setOnClickListeners(){
        btnLogout?.setOnClickListener {
            Log.d("CLICK", "CLICK ME")
            viewModel?.logoutUser()
            findNavController().navigate(R.id.action_profileFragment_to_navigation_home)
        }
        btnMeasureCp?.setOnClickListener {
            Log.d("CLICK", "CLICK ME")
            findNavController().navigate(R.id.action_profileFragment_to_cartoidPulseFragment)
        }
    }


    /**
     *
     */
    override fun getApplicationContext(): Context? {
        return context
    }
}
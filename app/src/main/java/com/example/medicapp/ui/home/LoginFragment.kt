package com.example.medicapp.ui.home

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import com.example.medicapp.MainActivity

import com.example.medicapp.R
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.firestore.auth.FirebaseAuthCredentialsProvider
import com.example.medicapp.ui.home.HomeViewModel as HomeViewModel

class LoginFragment : Fragment() {



    private val btnEnterData by lazy<MaterialButton?> {
        view?.findViewById(R.id.btn_enter_data_login)
    }

    val etPhone by lazy<TextInputEditText?> {
        view?.findViewById(R.id.tie_phone_login)
    }

    //private lateinit var auth: FirebaseAuthCredentialsProvider


    //private val loginViewModel: LoginViewModel? = null

    /*private val etEps by lazy<TextInputEditText?> {
        view?.findViewById(R.id.tie_eps_home)
    }*/

    companion object {
        fun newInstance() = LoginFragment()
    }

    private var viewModel: HomeViewModel? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        //auth= FirebaseAuthCredentialsProvider()

        viewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        return inflater.inflate(R.layout.login_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setOnClickListeners()
    }


    /*override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        // TODO: Use the ViewModel
    }*/

    private fun setOnClickListeners(){

        btnEnterData?.setOnClickListener{
            validateData()
            var bundle= bundleOf("phone" to etPhone?.text.toString())
            findNavController().navigate(R.id.action_loginFragment_to_profileFragment,bundle )

            /*var fm= fragmentManager
            //fm!!.beginTransaction().add(this.id, ProfileFragment(viewModel!!)).commit()
            ProfileFragment(viewModel!!).show(fm!!, "")*/
        }
    }

    /**
     * Checks if fields are empty and shows a Toast, if they are not empty, we should try to register the user.
     */
    private fun validateData() {
        if ( !TextUtils.isEmpty(etPhone?.text) ) {
            Log.d("aaaaaaaaaaaaaa", "viewModel: "+viewModel.toString())
            viewModel?.setPhoneTest(etPhone?.text.toString())

        } else {
            if (activity is MainActivity) (activity as MainActivity).showSnackBar(getString(R.string.check_fields_are_not_empty))
        }
    }

}

package com.example.medicapp.ui.dashboard

import android.os.Bundle
import android.text.Layout
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.medicapp.R
import com.example.medicapp.database.MedicineDatabase
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import com.example.medicapp.databinding.FragmentDashboardBinding


class DashboardFragment : Fragment() {

    private lateinit var dashboardViewModel: DashboardViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentDashboardBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_dashboard, container, false)

        val application = requireNotNull(this.activity).application
        val dataSource = MedicineDatabase.getInstance(application).medicineDao
        val viewModelFactory = DashboardViewModelFactory(dataSource, application)
        val dashboardViewModel =
            ViewModelProviders.of(
                this, viewModelFactory).get(DashboardViewModel::class.java)
        val textView: TextView = binding.root.findViewById(R.id.text_dashboard)
        dashboardViewModel.text.observe(this, Observer {
            textView.text = it
        })

        binding.setLifecycleOwner(this)
        binding.dashboardViewModel = dashboardViewModel
        dashboardViewModel.navigateToAddMedicine.observe(this, Observer {
                med ->
            med?.let {
                this.findNavController().navigate(
                    DashboardFragmentDirections
                        .actionNavigationDashboardToAddMedicineFragmet()
                )
                dashboardViewModel.doneNavigating()
            }
        })

        val adapter = MedicineListAdapter()
        binding.listMedicine.adapter = adapter

        dashboardViewModel.medicines.observe(viewLifecycleOwner, Observer {
            it?.let {
                adapter.data = it
            }
        })
        return binding.root
    }
}
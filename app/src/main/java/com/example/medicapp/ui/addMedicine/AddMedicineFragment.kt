package com.example.medicapp.ui.addMedicine

import android.app.TimePickerDialog
import android.os.Bundle
import android.text.Editable
import android.text.Layout
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.NotificationManagerCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.medicapp.R
import com.example.medicapp.database.MedicineDatabase
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import com.example.medicapp.Notifications.NotificationHelper.createNotificationChannel
import com.example.medicapp.databinding.AddMedicineFragmentBinding
import com.example.medicapp.ui.dashboard.DashboardFragmentDirections
import com.google.android.material.snackbar.Snackbar
import java.util.*


class AddMedicineFragment : Fragment() {

    private lateinit var addMedicineViewModel: AddMedicineViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: AddMedicineFragmentBinding = DataBindingUtil.inflate(
            inflater, R.layout.add_medicine_fragment, container, false)
        val view = inflater!!.inflate(R.layout.add_medicine_fragment, container, false)

        val application = requireNotNull(this.activity).application
        val dataSource = MedicineDatabase.getInstance(application).medicineDao
        val viewModelFactory = AddMedicineViewModelFactory(dataSource, application)
        val addMedicineViewModel =
            ViewModelProviders.of(
                this, viewModelFactory).get(AddMedicineViewModel::class.java)

        binding.setLifecycleOwner(this)
        binding.addMedicineViewModel = addMedicineViewModel
        val editTextName=view!!.findViewById<EditText>(R.id.tie_medicine)
        editTextName.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                Log.d(p0.toString(),p0.toString())
                addMedicineViewModel.changeMedicineNameString(p0.toString())
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                Log.d(p0.toString(),p0.toString())
                addMedicineViewModel.changeMedicineNameString(p0.toString())
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                Snackbar.make(
                    activity!!.findViewById(android.R.id.content),
                    p0!!,
                    Snackbar.LENGTH_SHORT // How long to display the message.
                ).show()
                addMedicineViewModel.changeMedicineNameString(p0.toString())
            }
        })
        val c = Calendar.getInstance()
        val hour = c.get(Calendar.HOUR_OF_DAY)
        val minute = c.get(Calendar.MINUTE)
        addMedicineViewModel.changeTimeString(hour.toString() + " : " + minute.toString(),hour,minute)
        addMedicineViewModel.changeMedicineNameString("prueba")
        addMedicineViewModel.showSnackBarEvent.observe(this, Observer {
            if (it == true) { // Observed state is true.
                Snackbar.make(
                    activity!!.findViewById(android.R.id.content),
                    getString(R.string.add_medicine),
                    Snackbar.LENGTH_SHORT // How long to display the message.
                ).show()
                addMedicineViewModel.doneShowingSnackbar()
            }
            val tpd = TimePickerDialog(this.context,TimePickerDialog.OnTimeSetListener(function = { view, h, m ->
                addMedicineViewModel.changeTimeString(h.toString() + " : " + m,h,m)
            }),hour,minute,false)
            tpd.show()
        })

        addMedicineViewModel.navigateToDashboard.observe(this, Observer {
                med ->
            med?.let {
                this.findNavController().navigate(
                    AddMedicineFragmentDirections
                        .actionAddMedicneFragmentToNavigationDashboard(med.medicineId)
                )
                addMedicineViewModel.doneNavigating()
            }
        })

        return binding.root
    }
}
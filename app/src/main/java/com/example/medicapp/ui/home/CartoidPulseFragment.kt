package com.example.medicapp.ui.home

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.annotation.Nullable
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.example.medicapp.R
import kotlinx.android.synthetic.main.fragment_carotid_pulse.view.*

class CartoidPulseFragment: Fragment(){

    private var viewModel: HomeViewModel? = null


    override fun onCreateView(
        inflater: LayoutInflater, @Nullable container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel =
            ViewModelProviders.of(this).get(HomeViewModel::class.java)
        return inflater.inflate(R.layout.fragment_carotid_pulse, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setOnClickListeners()
    }

    private val btnMeasureCP by lazy<Button?>{
        view?.findViewById(R.id.iv_measure_cp)
    }


    private var count = 0

    var rythm = 0


    private var startMillis: Long = 0


    private fun setOnClickListeners(){
        btnMeasureCP?.setOnClickListener {
            Log.d("CLICK", "CLICK ME")
            //count ++
            //calculateCartoidPulse()
            calculateCP()
        }
    }

    fun calculateCP(){


        var time: Long= System.currentTimeMillis()

        Log.d("mierda", "startMillis: "+startMillis)
        Log.d("mierda", "time - startMillis: "+(time - startMillis))

        if ((time - startMillis) >= 10000L && (time - startMillis) <= 11000L) {
            //do whatever you need
            viewModel?.updateUserCPById((count * 6).toDouble())
            Toast.makeText(context, "Su ritmo cardiaco es: "+(count * 6), Toast.LENGTH_LONG).show()
            Log.d("mierda", "contador: "+count)
        }
        if (startMillis == 0L || (time-startMillis> 10000) ) {
            startMillis=time
            count=1

        }
        //it is not the first, and it has been  less than 3 seconds since the first
        else{ //  time-startMillis< 3000
            count++
        }


    }


    fun calculateCartoidPulse (){
        val start = System.currentTimeMillis()
        Log.d("hola", "start: "+start)

        var tcount = 0


        while (tcount <= 22000){

            tcount = (System.currentTimeMillis()- start).toInt()
        }

        rythm = count*3
        count =0
    }

}
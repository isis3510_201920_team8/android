package com.example.medicapp.Notifications

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.medicapp.AppGlobalReceiver
import com.example.medicapp.MainActivity
import com.example.medicapp.R
import com.example.medicapp.database.Medicine

object NotificationHelper {

    private const val ADMINISTER_REQUEST_CODE = 2019

    /**
     * Sets up the notification channels for API 26+.
     * Note: This uses package name + channel name to create unique channelId's.
     *
     * @param context     application context
     * @param importance  importance level for the notificaiton channel
     * @param showBadge   whether the channel should have a notification badge
     * @param name        name for the notification channel
     * @param description description for the notification channel
     */
    fun createNotificationChannel(context: Context, importance: Int, showBadge: Boolean, name: String, description: String) {
        // 1
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            // 2
            val channelId = "${context.packageName}-$name"
            val channel = NotificationChannel(channelId, name, importance)
            channel.description = description
            channel.setShowBadge(showBadge)

            // 3
            val notificationManager = context.getSystemService(NotificationManager::class.java)
            notificationManager.createNotificationChannel(channel)
        }
    }

    fun createNotificationForMedicine(context: Context, reminderData: Medicine) {

        // create a group notification
        val groupBuilder = buildGroupNotification(context, reminderData)

        // create the pet notification
        val notificationBuilder = buildNotificationForMedicine(context, reminderData)

        // add an action to the pet notification
        val administerPendingIntent = createPendingIntentForAction(context, reminderData)
        notificationBuilder.addAction(R.drawable.common_google_signin_btn_icon_dark , context.getString(R.string.administer), administerPendingIntent)

        // call notify for both the group and the pet notification
        val notificationManager = NotificationManagerCompat.from(context)
        notificationManager.notify(1002, groupBuilder.build())
        notificationManager.notify(1001, notificationBuilder.build())
    }

    private fun buildGroupNotification(context: Context, reminderData: Medicine): NotificationCompat.Builder {
        val channelId = "${context.packageName}-${reminderData.name}"
        return NotificationCompat.Builder(context, channelId).apply {
            setSmallIcon(R.drawable.drugs)
            setContentTitle(reminderData.name)
            setContentText(context.getString(R.string.group_notification_for, reminderData.name))
            setStyle(NotificationCompat.BigTextStyle().bigText(context.getString(R.string.group_notification_for, reminderData.name)))
            setAutoCancel(true)
            setGroupSummary(true)
            setGroup(reminderData.name)
        }
    }

    private fun buildNotificationForMedicine(context: Context, reminderData: Medicine): NotificationCompat.Builder {


        val channelId = "${context.packageName}-${reminderData.name}"

        return NotificationCompat.Builder(context, channelId).apply {
            setSmallIcon(R.drawable.drugs)
            setContentTitle(reminderData.name)
            setAutoCancel(true)

            // get a drawable reference for the LargeIcon
            val drawable = R.drawable.drugs

            setLargeIcon(BitmapFactory.decodeResource(context.resources, drawable))
            setContentText("${reminderData.name}, ${reminderData.name}")
            setGroup(reminderData.name)

            // Launches the app to open the reminder edit screen when tapping the whole notification
            val intent = Intent(context, MainActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                putExtra("id", reminderData.medicineId)
            }

            val pendingIntent = PendingIntent.getActivity(context, 0, intent, 0)
            setContentIntent(pendingIntent)
        }
    }

    private fun createPendingIntentForAction(context: Context, reminderData: Medicine): PendingIntent? {
        /*
            Create an Intent to update the ReminderData if Administer action is clicked
         */
        val administerIntent = Intent(context, AppGlobalReceiver::class.java).apply {
            action = context.getString(R.string.action_medicine_administered)
            putExtra(AppGlobalReceiver.NOTIFICATION_ID, reminderData.medicineId)
            putExtra("id", reminderData.medicineId)
            putExtra("administered", true)
        }

        return PendingIntent.getBroadcast(context, ADMINISTER_REQUEST_CODE, administerIntent, PendingIntent.FLAG_UPDATE_CURRENT)
    }
}
package com.example.medicapp.models

import androidx.room.Entity

@Entity(primaryKeys = [("phoneNumber")])
data class User(
    val firstName: String,
    val lastName: String,
    val phoneNumber: String,
    val eps: String,
    val disease: String
   /* val mail: String,
    val password : String*/
){

    constructor(): this("",
        "",
        "",
        "",
        "")
    var heartRate: Double = 0.0
    var bloodPressure: Double = 0.0
    var saturation: Double = 0.0
}

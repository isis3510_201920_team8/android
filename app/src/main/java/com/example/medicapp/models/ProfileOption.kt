package com.example.medicapp.entities

data class ProfileOption(
    val category: String?,
    val value: String?,
    val image: Int,
    val profileOptionType: Int
)
package com.example.medicapp

import android.content.Context
import androidx.core.app.NotificationManagerCompat
import androidx.multidex.MultiDexApplication
import com.example.medicapp.Notifications.NotificationHelper
import com.example.medicapp.database.AppDatabase

class BaseApplication: MultiDexApplication() {

    companion object{
        var ctx: Context? = null
    }
    //toma de datos de la aplicacion para ser persistidos
    override fun onCreate() {
        super.onCreate()
        AppDatabase.getAppDataBase(this)
        ctx = applicationContext
        NotificationHelper.createNotificationChannel(this,
            NotificationManagerCompat.IMPORTANCE_HIGH, true,
            "Notification channel for medicines.", "Notification channel for medicines.")
    }
}
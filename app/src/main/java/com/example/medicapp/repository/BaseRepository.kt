package com.example.medicapp.repository

import com.example.medicapp.database.AppDatabase
import com.google.firebase.firestore.FirebaseFirestore

open class BaseRepository{
    val db = FirebaseFirestore.getInstance()
    val localDb = AppDatabase.INSTANCE
}
package com.example.medicapp.repository.user

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.medicapp.models.User
import com.example.medicapp.repository.BaseRepository


class UserDataSource: BaseRepository(), UserRepository{

    companion object {
        const val CLASS_NAME = "UserDataSource"
    }

    val response = MutableLiveData<Int>()
    private val userDao = localDb?.userDao()

    //datos del usuario
    override fun registerUserData(
        firstName: String,
        lastName: String,
        phoneNumber: String,
        eps: String,
        disease: String
    ) {
        val user = User(firstName, lastName, phoneNumber, eps, disease)
        if (userDao?.getUserByPhone(user.phoneNumber) == null) {
            userDao?.insertUser(user)
            val userMap = HashMap<String, Any>()
            userMap["first_name"] = firstName
            userMap["last_name"] = lastName
            userMap["phone_number"] = phoneNumber
            userMap["disease"] = disease
            db.collection("patients")
                .document(phoneNumber).set(userMap)
                .addOnSuccessListener {
                    response.value = UserConstants.USER_CREATED
                }.addOnFailureListener {
                    response.value = UserConstants.USER_NOT_CREATED_FAILURE
                    Log.d(CLASS_NAME, it.message ?: "Message or exception null")
                }
        } else {
            response.value = UserConstants.USER_ALREADY_EXISTS
        }
    }

    override fun logoutUser() {
        getUser()?.let { nonNullUser ->
            userDao?.deleteUser(nonNullUser)
        }
    }


    override fun getUser(): User? {
        return userDao?.getUser()
    }

    override fun getUser(phoneNumber:String): User? {


        userDao?.getUserByPhone(phoneNumber)
        val userMap = HashMap<String, Any>()
        db.collection("patients")
            .document(phoneNumber).get()
            .addOnSuccessListener { documentSnapshot ->

                Log.d("aaaaaaa", "paso por aqui")
                val d = documentSnapshot
                val p = d.toObject(User::class.java)

                userMap["first_name"] = p!!.firstName
                userMap["last_name"] = p!!.lastName
                userMap["phone_number"] = phoneNumber
                userMap["eps"]= p!!.eps
                userMap["disease"] = p!!.disease
               /* userMap["heartRate"] = p!!.heartRate
                userMap["bloodPleasure"]= p!!.bloodPressure
                userMap["saturation"]= p!!.saturation*/
            }.addOnFailureListener {
                response.value = UserConstants.USER_NOT_CREATED_FAILURE
                Log.d(CLASS_NAME, it.message ?: "Message or exception null")
            }
        var user: User? = User(userMap["first_name"].toString(),
            userMap["last_name"].toString(),
            phoneNumber,
            userMap["eps"].toString(),
            userMap["disease"].toString())

        /*if(userMap["heartRate"].toString() is String)
            user?.heartRate = userMap["heartRate"].toString().toDouble()

        if(userMap["bloodPreasure"].toString() is String)
            user?.bloodPressure= userMap["bloodPreasure"].toString().toDouble()

        if(userMap["saturation"].toString() is String)
            user?.saturation= userMap["saturation"].toString().toDouble()*/

        return user
    }

    override fun isUserLogged(): Boolean {
        return userDao?.getUser() != null
    }


    override fun updateUserCPById(phoneNumber: String, pulse: Double){
        userDao?.updateUserCPById(phoneNumber, pulse)
    }
}
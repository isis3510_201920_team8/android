package com.example.medicapp.repository.user

//constantes de la excepcion
object UserConstants {
    const val USER_CREATED = 200
    const val USER_NOT_CREATED_FAILURE = 203
    const val USER_ALREADY_EXISTS = 204
}
package com.example.medicapp.repository.user

import com.example.medicapp.models.User

interface UserRepository{
    //registrar un usuario
    fun registerUserData(firstName: String, lastName: String, phoneNumber: String, eps: String, disease: String)
    fun isUserLogged(): Boolean
    fun getUser(): User?
    fun logoutUser()
    fun updateUserCPById(phoneNumber: String, pulse: Double)
    fun getUser(phoneNumber: String): User?
    //fun updateCP(cp: Int)
}
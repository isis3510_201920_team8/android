package com.example.medicapp

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.core.app.NotificationManagerCompat
import com.example.medicapp.Notifications.NotificationHelper
import com.example.medicapp.ui.notifications.LocationUtils
import com.google.android.gms.location.FusedLocationProviderClient


class MedicApp : Application() {

    companion object {
        var ctx: Context? = null
        var loc: FusedLocationProviderClient? = null
    }

    override fun onCreate() {
        super.onCreate()
        ctx = applicationContext
        LocationUtils().getInstance(applicationContext)
        NotificationHelper.createNotificationChannel(this,
            NotificationManagerCompat.IMPORTANCE_HIGH, true,
            "Notification channel for medicines.", "Notification channel for medicines.")
    }

}